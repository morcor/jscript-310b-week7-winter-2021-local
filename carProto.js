/**
 * Car class
 * @constructor
 * @param {String} model
 */
class Car {
    constructor(model){
      this.currentSpeed = 0;
      this.model = model;
    }

    accelerate() {
      this.currentSpeed += 1;
      console.log(`Increasing speed by 1 - new speed is ${this.currentSpeed} mph`);
    }

    brake() {
      this.currentSpeed -= 1;
      console.log(`Decreasing speed by 1 - new speed is ${this.currentSpeed} mph`);
    }

    toString() {
      console.log(`${this.model} is currently going ${this.currentSpeed} mph`);
    }
    electricCarAccelerate() {
      this.currentSpeed += 2;
      console.log(`Increasing speed by 1 - new speed is ${this.currentSpeed} mph`)
    }
    reset(){
      this.currentSpeed = 0;
    }
  };

 
  let myCar = new Car("Audi");
  

 myCar.accelerate();
 myCar.accelerate();
 myCar.brake();
 myCar.toString();
 myCar.reset();
  

/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()


class Model extends Car{
    constructor(model, currentSpeed) {
      super('Tesla', model, currentSpeed)
      this.electricCar = true;
    }

};

myCar.model = "Tesla";

myCar.electricCarAccelerate();
myCar.electricCarAccelerate();
myCar.brake();
myCar.toString();



 