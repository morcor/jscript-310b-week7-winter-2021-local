describe("BlackJack Game Tests", () => {
    describe("calPoints Function", () => {

        it("should return a score and a isSoft value of false if no ace", () => {
            const hand = [
                {val: 3, displayVal: "3", suit: "hearts"},
                {val: 4, displayVal: "4", suit: "hearts"},
                {val: 5, displayVal: "5", suit: "hearts"},
            ];

            const result = calcPoints(hand);

            expect(result).toEqual({
                total: 12,
                isSoft: false
            })

        })
        it("should return a score and a isSoft value of true if there is one ace", () => {

        })
        it("should return a score and not soft if there is one ace and ", () => {

        })

    })
})